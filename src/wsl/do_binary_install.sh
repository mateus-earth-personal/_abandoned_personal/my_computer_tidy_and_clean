#!/usr/bin/env bash

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Vars                                                                       ##
##----------------------------------------------------------------------------##
SCRIPT_DIR="$(pw_get_script_dir)";
USER_HOME="$(pw_find_real_user_home)";
INSTALL_PATH="${USER_HOME}/.stdmatt/bin";
TEMP_PATH="/tmp";
PROFILE="$(pw_get_default_bashrc_or_profile)";
CURR_OS="$(pw_os_get_simple_name)";


##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
install_cmake()
{
    test -n "$(pw_get_program_path cmake)"    \
        && pw_func_log "Already installed..." \
        && return 0;

    cd "${INSTALL_PATH}";

    ## Download and extract...
    local URL="https://github.com/Kitware/CMake/releases/download/v3.17.0/cmake-3.17.0-Linux-x86_64.tar.gz";
    pw_network_simple_url_downloader "${URL}" "${TEMP_PATH}/cmake.tar.gz";
    tar -xvf "${TEMP_PATH}/cmake.tar.gz" -C "${INSTALL_PATH}";

    ## Add to the PATH.
    echo "## CMake ##"                                                   >> "$PROFILE";
    echo 'PATH="$PATH:'"${INSTALL_PATH}/cmake-3.17.0-Linux-x86_64/bin\"" >> "$PROFILE";

    cd -;
}

## @TODO(stdmatt): Ignoring the emscripten by now...
##------------------------------------------------------------------------------
install_emscripten()
{
    echo "Ignoring emscripten....";
    # test -n "$(pw_get_program_path em++)"     \
    #     && pw_func_log "Already installed..." \
    #     && return 0;


    # cd "${INSTALL_PATH}";

    # ## Get the repo...
    # git clone https://github.com/emscripten-core/emsdk.git
    # cd emsdk

    # git pull
    # ./emsdk install  latest
    # ./emsdk activate latest

    # echo "## Emscripten ##"                                      >> "$PROFILE";
    # echo "source ${INSTALL_PATH}/emsdk/emsdk_env.sh > /dev/null" >> "$PROFILE";
}


## @TODO(stdmatt): Ignoring the vswhere by now...
##------------------------------------------------------------------------------
install_vswhere()
{
    echo "Ignoring vswhere....";
    # USER_BIN_DIR="${USER_HOME}/.stdmatt_bin/wsl";
    # URL_VSWHERE="https://github.com/microsoft/vswhere/releases/download/2.8.4/vswhere.exe";

    # ##
    # ## Install stuff for Windows (WSL).
    # pw_network_simple_url_downloader "$URL_VSWHERE" "vswhere.exe";

    # mkdir -vp "$USER_BIN_DIR";
    # cp -vf vswhere.exe "$USER_BIN_DIR";
    # chmod 744 "${USER_BIN_DIR}/vswhere.exe";
}


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
echo "$(pw_FM "--: Innstalling binary packages ($CURR_OS) :--")";
mkdir -p "${INSTALL_PATH}";

install_cmake;
install_emscripten;
