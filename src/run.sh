#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : run.sh                                                        ##
##  Project   : my_computer_tidy_and_clean                                    ##
##  Date      : Feb 24, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
PROGRAM_NAME="mctac";
PROGRAM_VERSION="1.2.0";
PROGRAM_COPYRIGHT_YEARS="2020";

_FLAG_FIRST_INSTALL="--first-install";
_FLAG_UPDATE_TOOLS="--update-tools";
_FLAG_UPDATE_SYSTEM="--update-system";
_FLAG_SETUP_EXTRAS="--setup-extras";
_FLAG_CLONE_REPOS="--clone-repos";

##----------------------------------------------------------------------------##
## Variables                                                                  ##
##----------------------------------------------------------------------------##
_ARG_FIRST_INSTALL="";
_ARG_UPDATE_TOOLS="";
_ARG_UPDATE_SYSTEM="";
_ARG_SETUP_EXTRAS="";
_ARG_CLONE_REPOS="";

##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
_get_script_dir()
{
   local SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[1]}")" && pwd)";
   echo "$SCRIPT_DIR"
}

##------------------------------------------------------------------------------
_show_help()
{
    cat << HELP_MSG
Usage:
  $PROGRAM_NAME [--help] [--version]
  $PROGRAM_NAME [$_FLAG_FIRST_INSTALL]
  $PROGRAM_NAME [$_FLAG_UPDATE_TOOLS] [$_FLAG_UPDATE_SYSTEM] [$_FLAG_SETUP_EXTRAS]
  $PROGRAM_NAME [$_FLAG_CLONE_REPOS]

General:
  *--help    - Show this screen.
  *--version - Show program version and copyright.

First time:
  *$_FLAG_FIRST_INSTALL - Implies $_FLAG_UPDATE_TOOLS, $_FLAG_UPDATE_SYSTEM,
                             $_FLAG_SETUP_EXTRAS, $_FLAG_CLONE_REPOS.
                     It also will install shellscript_utils from scratch.

Update: (runs ./src/base/do_update.sh)
  $_FLAG_UPDATE_TOOLS  - Update all programs developed by myself.
  $_FLAG_UPDATE_SYSTEM - Update the platform specific system things.

Extras: (runs ./src/base/do_extras.sh)
  $_FLAG_SETUP_EXTRAS - Update / Setup all things that are platform independent,
                   like pip, npm, paths, etc...

Repositories: (runs ./src/base/do_clone_repos.sh)
  $_FLAG_CLONE_REPOS - Clone all the my repositories from gitlab.

Notes:
  If no arguments are given $PROGRAM_NAME will assume $_FLAG_FIRST_INSTALL.

  Options marked with * are exclusive, i.e. the $PROGRAM_NAME will run that
  and exit after the operation.
HELP_MSG
}


##------------------------------------------------------------------------------
_show_version()
{
    cat << VERSION_MSG
$PROGRAM_NAME - $PROGRAM_VERSION - stdmatt <stdmatt@pixelwizards.io>
Copyright (c) $PROGRAM_COPYRIGHT_YEARS - stdmatt
This is a free software (GPLv3) - Share/Hack it
Check http://stdmatt.com for more :)
VERSION_MSG
}

##------------------------------------------------------------------------------
_set_flag_first_install()
{
    _ARG_UPDATE_TOOLS="true";
    _ARG_UPDATE_SYSTEM="true";
    _ARG_SETUP_EXTRAS="true";
    _ARG_CLONE_REPOS="true";
}

##------------------------------------------------------------------------------
_check_for_cmd_line_flags()
{
    test ${#@} == 0 && _set_flag_first_install;

    for ARG in $@; do
        ## Help and Version.
        if [ "$ARG" == "--help" ]; then
            _show_help;
            exit 0;
        fi;
        if [ "$ARG" == "--version" ]; then
            _show_version;
            exit 0;
        fi;

        ## First install.
        if [ "$ARG" == "$_FLAG_FIRST_INSTALL" ]; then
            _set_flag_first_install;
            return;
        fi;

        ## Other flags.
        if [ "$ARG" == "$_FLAG_UPDATE_TOOLS" ] ; then
            _ARG_UPDATE_TOOLS="true";
        elif [ "$ARG" == "$_FLAG_UPDATE_SYSTEM" ] ; then
            _ARG_UPDATE_SYSTEM="true";
        elif [ "$ARG" == "$_FLAG_SETUP_EXTRAS" ] ; then
            _ARG_SETUP_EXTRAS="true";
        elif [ "$ARG" == "$_FLAG_CLONE_REPOS" ] ; then
            _ARG_CLONE_REPOS="true";
        fi;
    done;
}

##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
_check_for_cmd_line_flags $@;
echo "_ARG_FIRST_INSTALL  $_ARG_FIRST_INSTALL";
echo "_ARG_CLONE_REPOS    $_ARG_CLONE_REPOS";
echo "_ARG_UPDATE_TOOLS   $_ARG_UPDATE_TOOLS";
echo "_ARG_UPDATE_SYSTEM  $_ARG_UPDATE_SYSTEM";
echo "_ARG_SETUP_EXTRAS   $_ARG_SETUP_EXTRAS";


SCRIPT_DIR=$(_get_script_dir);
test -n "$_ARG_FIRST_INSTALL" && "${SCRIPT_DIR}/base/do_pre_install.sh";
test -n "$_ARG_FIRST_INSTALL" && "${SCRIPT_DIR}/base/do_post_install.sh";
test -n "$_ARG_CLONE_REPOS"   && "${SCRIPT_DIR}/base/do_clone_repos.py";
test -n "$_ARG_UPDATE_TOOLS"  && "${SCRIPT_DIR}/base/do_update.sh" --tools;
test -n "$_ARG_UPDATE_SYSTEM" && "${SCRIPT_DIR}/base/do_update.sh" --system;
test -n "$_ARG_SETUP_EXTRAS"  && "${SCRIPT_DIR}/base/do_extras.sh";
