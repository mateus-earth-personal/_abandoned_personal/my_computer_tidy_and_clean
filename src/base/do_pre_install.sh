#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : do_pre_install.sh                                             ##
##  Project   : my_computer_tidy_and_clean                                    ##
##  Date      : Feb 24, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Pre-Install                                                                ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
install_shellscript_utils()
{
    echo "Installing shellscript-utils...";

    local REPO_URL="https://gitlab.com/stdmatt-libs/shellscript_utils";
    local TMP_DIR="/tmp";

    rm -rf "$TMP_DIR/shellscript_utils";
    cd "$TMP_DIR";

    git clone "$REPO_URL";
    cd shellscript_utils;
    echo "shellscript_utils is clonned at ($(pwd))";

    ## @XXX: Is reliable to check for sudo in all platforms?
    local FAKE_SUDO=$(which sudo);
    $FAKE_SUDO ./install.sh;

    cd -
}

install_shellscript_utils;
