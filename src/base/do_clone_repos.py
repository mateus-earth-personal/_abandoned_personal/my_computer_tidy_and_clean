#!/usr/bin/env python3
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : do_clone_repos.py                                             ##
##  Project   : my_computer_tidy_and_clean                                    ##
##  Date      : Feb 24, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
import pdb;
import json;
import os.path;
import os;
import sys;
import urllib.request;


##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
## @TODO(stdmatt): Find a way to automatically get the groups that I'm into...
GROUPS=[
    "stdmatt-demos",
    "stdmatt-libs",
    "stdmatt-personal",
    "stdmatt-games",
    "stdmatt-tools",
    "stdmatt-3rd-party"
];

URL_FMT="https://gitlab.com/api/v4/groups/{name}/projects?include_subgroups=true";
BASE_PATH="~/Documents/Projects/stdmatt";


##----------------------------------------------------------------------------##
## Helper Functions                                                           ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def clone(url, path):
    os.system("git clone --recurse-submodules {url} {path}".format(url=url, path=path));


##----------------------------------------------------------------------------##
## Entry Point                                                                ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def main():
    clone_path = sys.argv[1:];
    if(len(clone_path) == 0):
        clone_path = BASE_PATH;
    else:
        clone_path = clone_path[0];

    for group_name in GROUPS:
        url      = URL_FMT.format(name=group_name);
        response = urllib.request.urlopen(url);
        data     = json.loads(response.read());

        group_name_dir = os.path.join(
            clone_path,
            group_name.replace("stdmatt-", "")
        );

        for item in data:
            name      = item["name"];
            namespace = item["namespace"]["full_path"];
            repo_url  = item["http_url_to_repo"];

            ## If the namespace is not a subgroup of this group
            ## we don't want to have it's name because we're clonning
            ## and creating the directories based on the group's names already.
            namespace = os.path.basename(namespace);
            namespace = "" if namespace == group_name else namespace;

            clone_dir = os.path.abspath(os.path.expanduser(os.path.join(group_name_dir, namespace, name)));
            if(os.path.isdir(clone_dir)):
                print("Project {name} is already clonned at ({dir})".format(name=name, dir=clone_dir));
                continue;

            os.makedirs(clone_dir, exist_ok=True);
            clone(repo_url, clone_dir);

main();
